libtest-redisserver-perl (0.23-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 10.
  * Set Testsuite header for perl package.

  [ Étienne Mollier ]
  * Import upstream version 0.23.
  * redis-error-message.patch removed: applied upstream.
  * d/control: declare compliance to standards version 4.6.1.
  * d/control: the package now depends on libredis-perl.
  * d/control: Rules-Requires-Root: no.

 -- Étienne Mollier <emollier@debian.org>  Thu, 26 May 2022 20:19:28 +0200

libtest-redisserver-perl (0.21-2) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ gregor herrmann ]
  * Add patch to fix test failure in t/unknown_conf.t.
    Apparently the error message in Redis 6.0.5 changed, so update the regexp
    in the test.
    Thanks to Lucas Nussbaum for the bug report. (Closes: #963357)

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Jun 2020 23:19:20 +0200

libtest-redisserver-perl (0.21-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Relax to build-depend unversioned on cdbs.
    + Stop build-depend on devscripts.
      Closes: Bug#868945. Thanks to Lucas Nussbaum.
  * Update watch file:
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
    + Use substitution strings.
  * Update git-buildpackage config: Filter any .gitignore file.
  * Update copyright info:
    + Use https protocol in initial format line.
    + Drop superfluous copyright signs.
    + Extend coverage for myself.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use git (not cgit) in path.
    + Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Sep 2017 14:16:27 +0200

libtest-redisserver-perl (0.20-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#815274.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Feb 2016 15:58:08 +0100
